import React from "react";
const toDoList=(props)=>{
    return ( 
    <div className="lidiv"> 
    <li className="li"><p className="liitem">{props.text}</p></li>
    <button className="delbtn" onClick={()=>{props.onSelect(props.id)}}>Delete</button>
    <br/>
    </div>
    )
}
export default toDoList;