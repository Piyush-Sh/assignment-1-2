import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import ToList from "./TodoList";
import "./app.css";
import Input from './Input';
import Button from './button';

const App=()=>{
    const [newInputData,setNewInputData]=useState(null);
    const [list, setList] = useState([]);
    
    function getValue(event){
        setNewInputData(event.target.value);
        
    };
    const updateData=()=>{
        setList((objitem)=>{
            return [...objitem,newInputData];
        })
        setNewInputData(""); //For empty the input box  with provide value 
    };
    const deleteEle=(index)=>{
        setList((objitem)=>{
            return objitem.slice(0,index).concat(objitem.slice(index+1,objitem.length));
        })    
    }
    return (
    <div className="Divbody">
        <Input data={newInputData} value={getValue}/>
        <ol className="show">
            { list.map((listitems,index)=>{
                return <ToList 
                text={listitems}
                key={index}
                id={index}
                onSelect={deleteEle}
                />
            })}
        </ol>
        <Button data={updateData}/>
    </div>
    );
};

ReactDOM.render(<App />,document.querySelector('#root'));