import React from "react";

const inputChild=(props)=>{
    return(
        <div>
        <label htmlFor="input" className="itemlabel">Enter Item  </label>
        <input className="item" value={props.input1} id="input" onChange={props.input2} />
        </div>
    )
}
export default inputChild